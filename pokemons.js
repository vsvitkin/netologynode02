const fs = require('fs');

class Pokemon {
  constructor (name, power) {
    this.name=name;
    this.power=power;
  }
  show(){
    console.log(`Hello, I'm pokemon ${this.name}, my power is ${this.power}`);
  }
  valueOf(){
    return this.power;
  }
  toFile(file, callback){
    fs.writeFile(file, this.name+'|'+this.power, err=>{callback(err)});
  }
}

class PokemonList extends Array{
  constructor (...pokemons){
    super();
    pokemons.forEach(p=>{this.push(p)});
  }
  add(name, power){
    this.push(new Pokemon(name, power));
  }
  show(){
    console.log('Count of pokemons in list : '+this.length);
    this.forEach(p=>{p.show();});
  }
  max(){
    let pokemon=null;
    this.forEach(p=>{
      if ((pokemon==null)||(p>pokemon)){
        pokemon=p;
      }});
    return pokemon;
  }
  fromJson(fname){
    this.length=0;
    let listJson = require(fname);
    let added=0;
    for(let one of listJson){
      if (one['name']&&one['power']){
        added++;
        p.add(one['name'],one['power']);
      }
    }
    if (added==0){
      throw(new Error('Trash in list file'));
    }
  }
}

module.exports = {
  Pokemon,
  PokemonList
}
