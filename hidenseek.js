const fs = require('fs');
const PokemonList = require('./pokemons').PokemonList;

const hide = (path, pokemonList, callback) => {
    console.log('Preparing folders...');
    createDirStructure(path, false, 11, (err) => {
        if (err) {
            callback(err, null);
        } else {
            console.log('Hiding pokemons...');
            let pokemonsToHide = randomSubset(pokemonList);
            let foldersToHide = randomArray(pokemonsToHide.length, 0, 9);
            hidePokemonsToFolders(pokemonsToHide, foldersToHide, path, callback);
        }
    });
}

const seek = (path, callback) => {
    console.log('Trying to find some pokemons...');
    p = new PokemonList();
    var folders = 10;
    for (let i = 0; i < 10; i++) {
        let pathForCreating = nthFolderName(path, i);
        let fname = pathForCreating + '/pokemon.txt';
        fs.readFile(fname, (err, data) => {
            if (!err) {
                fs.unlink(fname);
                console.log(`I've found pokemon in ${pathForCreating}.`);
                let r = new String(data).split('|');
                if (r.length == 2) {
                    p.add(r[0], r[1]);
                }
            }
            if (--folders == 0) {
                callback(null, p);
            }
        });
    }
}


function randomInt(min, max) {
    return Math.round(Math.random() * (max - min) + min);
}

function randomArray(n, min, max) {
    let result = Array();
    for (let i = 0; i < n; i++) {
        do {
            var value = randomInt(min, max);
        } while (result.includes(value));
        result.push(value);
    }
    return result;
}

function randomSubset(pokemonList) {
    let res = new PokemonList();
    var n = randomInt(1, Math.min(3, pokemonList.length));
    let idsToHide = randomArray(n, 0, pokemonList.length - 1);
    for (id of idsToHide) {
        res.push(pokemonList[id]);
    }
    return res;
}

function hidePokemonsToFolders(pokemons, folders, path, callback){
  var error = false;
  let todo = pokemons.length;

  for (pokemon of pokemons) {
      pokemon.toFile(nthFolderName(path, folders[todo-1]) + '/pokemon.txt', err => {
          error &= Boolean(err);
          if (--todo == 0) {
              callback(error ? new Error('Error while hiding') : null, pokemons);
          }
      });
  }
}

function nthFolderName(path, n) {
    return path + ((++n < 10) ? '0' + n : n);
}

function createDirStructure(path, error, n, callback) {
    if (n == 0) {
        callback(error ? new Error('Error while making folders') : null);
    } else {
        fs.mkdir(n > 10 ? path : nthFolderName(path, n), err => {
            createDirStructure(path, error && err.code != 'EEXIST', n - 1, callback);
        });
    }
}

// Экспорт функций
module.exports = {
    hide,
    seek
}
