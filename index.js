/************************
Vasily Svitkin 2017
Netology NodeJS course
Home work 2
*/

const hs = require('./hidenseek');
const Pokemon = require('./pokemons').Pokemon;
const PokemonList = require('./pokemons').PokemonList;

const usageInstructions = 'Use: \nnode index hide [folder] [list] OR node index show [folder]\nExample:\nnode index hide ./field/ ./list.json\nnode index seek ./field/';

if ((process.argv.length < 4 || process.argv.length > 5)||(process.argv[2]=='hide'&&!process.argv[4])||(process.argv[2]!='hide'&&process.argv[2]!='seek')){
console.log(usageInstructions);
return;
}

var path = process.argv[3].slice(-1)=='/'?process.argv[3]:process.argv[3]+'/';
switch (process.argv[2]) {
  case 'hide':
    p = new PokemonList();
    p.fromJson(process.argv[4]);
    hs.hide(path, p, processResult);
    break;
  case 'seek':
    hs.seek(path, processResult);
  break;
}

function processResult(err, list) {
  if (err){
    throw(err);
  } else {
    console.log('------------------------RESULT------------------------');
    list.show();
  }
}
